pageextension 60102 "TCN_CustomerListExtPru" extends "Customer List" //MyTargetPageId
{
    layout
    {
        modify("Balance (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Balance Due (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Sales (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Payments (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        addlast(Control1)
        {
            field(ImportePedidosFacturas; cuTCNRegistrosVarios.ImportePedidosFacturasClienteF(Rec, false))
            {
                Caption = 'Importe de pedidos y facturas';
                trigger OnDrillDown()
                begin
                    cuTCNRegistrosVarios.ImportePedidosFacturasClienteF(Rec, true);
                end;
            }
        }
    }
    actions
    {
        addlast(Creation)
        {
            action(GetFilterNo) // Devuelve lo que hay dentro del filtro especificado
            {
                Caption = 'Ejemplo GetFilter campo Nº';
                trigger OnAction()
                begin
                    Message(GetFilter("No."));
                end;
            }

            action(GetFilters) // Nos devuelve una cadena con los filtros que se estén aplicando en ese momento en la página
            {
                Caption = 'Ejemplo GetFilters';
                trigger OnAction()
                begin
                    Message(GetFilters);
                end;
            }
        }
        addlast(Reporting)
        {
            action(Ocultar)
            {
                Image = BreakRulesOff;
                Caption = 'Ocultar campos calculados';
                Visible = xMostrarCalculados;
                trigger OnAction()
                begin
                    xMostrarCalculados := false;
                end;
            }
            action(Mostrar)
            {
                Image = BreakRulesOn;
                Caption = 'Mostrar campos calculados';
                Visible = not xMostrarCalculados;
                trigger OnAction()
                begin
                    xMostrarCalculados := true;
                end;
            }
        }
    }
    var
        cuTCNRegistrosVarios: Codeunit TCN_RegistrosVarios;
        xMostrarCalculados: Boolean;
}