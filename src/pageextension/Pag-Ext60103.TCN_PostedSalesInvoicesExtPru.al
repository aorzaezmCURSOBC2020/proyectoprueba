pageextension 60103 "TCN_PostedSalesInvoicesExtPru" extends "Posted Sales Invoices" //MyTargetPageId
{
    actions
    {
        addlast(Creation)
        {
            action(PruebaImpresion)
            {
                Caption = 'Imprimir';
                Image = PrintCheck;
                trigger OnAction()
                var
                    rlTempSalesInvoiceHeaderTMP: Record "Sales Invoice Header" temporary;
                    rlSalesInvoiceHeader: Record "Sales Invoice Header";
                    xlFiltro: Text;
                begin
                    // Solución 1. Forma más correcta
                    rlSalesInvoiceHeader := Rec;
                    rlSalesInvoiceHeader.Find();
                    rlSalesInvoiceHeader.SetRecFilter();
                    Report.Run(Report::"Sales - Invoice", true, false, rlSalesInvoiceHeader);
                    // 1er parámetro: El informe que queremos ejecutar
                    // 2o parámetro: Decimos que haga la impresión en la impresora por defecto
                    // 4o parámetro: Objeto que queremos imprimir

                    // // Solución 2
                    // xlFiltro := GetView();
                    // // El GetView recoge una consulta SQL
                    // Message(xlFiltro);
                    // SetRecFilter();
                    // // Te aplica un filtro para ese registro en concreto
                    // // En otras palabras, te aplica los filtros necesarios para el registro que se tenga seleccionado en la lista
                    // Report.Run(Report::"Sales - Invoice", true, false, Rec);
                    // Reset();
                    // SetView(xlFiltro);

                    // // Solución 03
                    // rlTempSalesInvoiceHeaderTMP.CopyFilters(Rec);
                    // SetRecFilter();
                    // Report.Run(Report::"Sales - Invoice", true, false, Rec);
                    // Reset();
                    // CopyFilters(rlTempSalesInvoiceHeaderTMP);

                end;
            }
        }
    }
}