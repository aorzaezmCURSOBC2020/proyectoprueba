pageextension 60104 "TCN_CustomerLookupExtPru" extends "Customer Lookup" //MyTargetPageId
{
    layout
    {
        modify("Credit Limit (LCY)")
        {
            Visible = xCreditoMaximoVisible;
        }
        modify("Phone No.")
        {
            Visible = false;
        }
    }
    procedure GetSelectionFilterF(var prCustomer: Record Customer)
    begin
        SetSelectionFilter(prCustomer);
    end;

    procedure VisibilidadCreditoMaximoF(pVisible: Boolean)
    var

    begin
        xCreditoMaximoVisible := pVisible;
    end;

    var
        xCreditoMaximoVisible: Boolean;
}