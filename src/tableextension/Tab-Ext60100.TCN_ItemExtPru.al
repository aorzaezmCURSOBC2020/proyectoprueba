tableextension 60100 "TCN_ItemExtPru" extends Item //MyTargetTableId
{
    fields
    {
        field(60100; CodAlmacen2; Code[10])
        {
            Caption = 'Cód. almacén 2';
            TableRelation = Location.Code;
        }
        modify(Description)
        {
            trigger OnAfterValidate()
            begin
                Description := LowerCase(Description);
                // En este caso utilizamos un trigger, ya que una suscripción es más lenta y 
                // realentiza el programa a la larga, ya que está despierto continuamente
            end;
        }
    }
}