codeunit 60102 "TCN_Subs02"
{
    SingleInstance = true;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF', '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoFCodeunitTCNRegistrosVariosF(pIdSesion: Integer; pMomentoFinalizacion: DateTime)
    begin
        Message('%1-%2', pIdSesion, pMomentoFinalizacion);
    end;
}