codeunit 60100 "TCN_RegistrosVarios"
{
    // m -> Modificar, d -> Borrar, i-> Insertar.
    Permissions = tabledata "Sales Invoice Header" = mdi,
    tabledata "Sales Invoice Line" = mdi;
    SingleInstance = true;
    // Esta propiedad se usa para usar una variable en varios objetos de BC.
    // En este ejemplo creamos los getter y setter, y posteriormente creamos un objeto
    // tipo codeunit en los objetos con los que voy a utilizar esta variable.
    // Se inicializa la codeunit como variable global para acceder al mismo objeto.
    // Si no creo esta propiedad, al cerrar las páginas se muere la inicialización del objeto
    // y con ello el valor.    

    var
        xValor: Text;


    trigger OnRun()
    var
        rlTempTempBlobTMP: Record TempBlob temporary;
        xlInStream: InStream;
        xlFichero: Text;
        xlLinea: Text;
        xlCampo: Text;
        xlNumCampo: Integer;
        rlCustomer: Record Customer;

        Ventana: Dialog;
        xlNumeroVeces: Integer;
        xSeleccion: Integer;
    begin
        // ExportacionDatosF();
        // ManejoFicherosF();
        // GruposFiltrosF();
        // GrupoFiltro2F();
        // EjemploCalcSumF();
        EjemploExcelF();
        exit;
        // Error('Ejecutando Cu');
        // EjecucionSegundoPlanoF();

        // ***EJEMPLO DE CONFIRM***
        // if Confirm('¿Confirma que desea registrar el registro actual?', false) then begin
        //     Message('Registro realizado');
        // end else begin
        //     Error('Proceso cancelado por el usuario');
        // end;

        // ***EJEMPLO STRMENU***
        // xSeleccion := StrMenu('&Enviar,&Facturar,Enviar &y Facturar', 2, '¿Qué desea hacer?');
        // Message(Format(xSeleccion));    

        // ***EJEMPLO PROGRESSBAR***
        // xlNumeroVeces := 1000000;
        // if GuiAllowed then begin
        //     Ventana.OPEN('Procesando @1@@@@@@@@');
        //     for xSeleccion := 1 to xlNumeroVeces do begin
        //         if GuiAllowed then begin
        //             Ventana.Update(1, Round(xSeleccion / xlNumeroVeces * 10000, 1));
        //         end;
        //     end;
        //     if GuiAllowed then begin
        //         Ventana.Close();
        //     end;
        // end;
        rlTempTempBlobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);
        // Aquí tengo creada la variable xlInStream que sirve para leer ficheros, entonces creo el flujo con la variable donde la vamos a guardar
        if UploadIntoStream('Seleccione el fichero', '', 'Archivos de texto (*.txt, *.csv)|*.txt;*.csv|Todos los archivos (*.*)|*.*', xlFichero, xlInStream) then begin
            while not xlInStream.EOS do begin
                // Con xlInStream.EOS nos referimos a que haga lo que hay debajo mientras no se termine el fichero
                xlInStream.ReadText(xlLinea);
                // Guarda en nuestra variable xlLinea la línea actual del fichero
                xlNumCampo := 0;
                rlCustomer.Init();
                foreach xlCampo in xlLinea.Split(';') do begin
                    xlNumCampo += 1;
                    case xlNumCampo of
                        1:
                            rlCustomer.Validate("No.", xlCampo);
                            // Validate siempre que queremos insertar un dato en una registro
                        2:
                            rlCustomer.Validate(Name, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Name)));
                            // En este caso, al ser un texto, se indica que se copie el string introducido desde la posición 1 hasta la máxima longitud del campo de la tabla
                        3:
                            rlCustomer.Validate(Address, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Address)));
                        4:
                            rlCustomer.Validate(City, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.City)));
                    end;
                end;
                rlCustomer.Insert(true);
                // Importante poner insert true porque crea la configuración mínima para que navision/bc pueda funcionar correctamente
            end;
        end else begin
            Error('No se pudo subir el fichero');
        end;

    end;

    procedure SetValorF(pValor: Text)
    begin
        xValor := pValor;
        Message('Valor %1 memorizado', xValor);
    end;

    procedure GetValorF(): Text
    begin
        exit(xValor);
    end;

    local procedure RefistrarEntradasF() xSalida: Integer
    begin

    end;

    [TryFunction]
    local procedure EjemploTry()
    begin

    end;

    local procedure EjemploArraysF()
    var
        mlArrayEjemplo: array[100, 200] of Text;
        mlArray2: array[100, 200] of Text;
        i: Integer;
        x: Integer;
        // Bucle para recorrerse un array
        // Ctrl + l para que el cursor vaya a la declaración de variables locales
        // Ctrl + g para que el cursor vaya a la declaración de variables globales 
    begin
        for i := 1 to ArrayLen(mlArrayEjemplo[1]) do begin
            for x := 1 to ArrayLen(mlArrayEjemplo[2]) do begin
                mlArrayEjemplo[i, x] := 'h';
            end;
        end;
        CopyArray(mlArray2, mlArrayEjemplo, 1);
        // Se indica el nuevo array, el que quieres copiar en segundo lugar y en la 3a posición la dimensión que quieres copiar
        // CompressArray() para comprimir un array que tiene celdas vacías
    end;

    // Vamos a crear 3 funciones para explicar la sobrecarga de funciones, en la que vamos a meter diferentes
    // valores. Se ejecutará a la que llamemos dependiendo de la variable que introducimos
    procedure SobreCargaF(pParametro: BigInteger) xSalida: Text;
    begin
        xSalida := Format(pParametro + 1);
        // Format() te convierte en texto lo que haya dentro
    end;

    procedure SobreCargaF(pParametro: Date) xSalida: Text;
    begin
        xSalida := Format(CalcDate('+1D', pParametro));
        // Esta función te suma un día. En el primer parámetro indicas el tiempo que quieres sumar/restar
        // y en el segundo la fecha con la que queremos trabajar
    end;

    procedure SobreCargaF(pParametro: date; pFormulaFecha: Text) xSalida: Text;
    begin
        xSalida := Format(CalcDate(pFormulaFecha, pParametro));
    end;

    procedure EjecucionSegundoPlanoF()
    var
        i: Integer;
        Ventana: Dialog;
        // Vamos a crear una función que tarde mucho en ejecutarse, para evitar la desesperación del cliente
        // creamos una ventana que indicara la carga del procedimiento
        rlAve: Record TCN_Ave;
        clMensaje: Label 'Sesión 2o plano finalizada';
        // Creamos estas variables para ver cuando termina la sesión en segundo plano
    begin
        if GuiAllowed then begin
            Ventana.Open('#1########');
        end;
        // Con @ se haría una progressbar
        for i := 1 to 9999999 do begin
            if GuiAllowed then begin
                // Al ejecutarse en segundo plano hay que preguntar que si tiene permitida la interfaz
                Ventana.Update(1, i);
            end;
        end;
        // rlAve.Init();
        // Iniciamos el registro
        // rlAve.Validate(Codigo, Format(SessionId()));
        // Validamos que el id de sesión podemos insertarlo en el código
        // rlAve.Validate(Nombre, clMensaje);
        // rlAve.Insert(true);    
        OnFinEjecucionSegundoPlanoF(CurrentDateTime, SessionId());
    end;
    // Se crea una publicación y justo arriba se llama en el momento que queremos
    // Al crear la publicación, se enviará la variable a todos los suscriptores
    [IntegrationEvent(false, false)]
    local procedure OnFinEjecucionSegundoPlanoF(pMomentoFinalizacion: DateTime; pIdSesion: Integer)
    // No se puede meter código
    begin
    end;

    local procedure PruebaVentanaF()
    var
        Ventana: Dialog;
        xlFila: Integer;
        xlColumna: Integer;
    begin
        if GuiAllowed then begin
            Ventana.Open('Fila... #1####\' + 'Columna... #2#####');
            // Aquí se crea el cuerpo de la ventana, siempre que se declaran variables tiene que ser después
            // de # y seguido de más de estas (determinan la longitud de la ventana)
        end;

        for xlFila := 1 to 1000 do begin
            for xlColumna := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(1, xlFila);
                    Ventana.Update(2, xlColumna);
                end;
            end;
        end;
    end;

    local procedure ExportacionDatosF()
    // En este procedimiento vamos a escribir campos de un registro en un archivo
    // Creamos un registro temporal (BLOB) y 
    var
        xlFichero: Text;
        rlVendor: Record Vendor;
        rlTempTempBlob: Record TempBlob temporary;
        xlOutStream: OutStream;
        xlInStream: inStream;
    begin
        xlFichero := 'Proveedores.csv';
        rlTempTempBlob.Blob.CreateOutStream(xlOutStream);
        rlTempTempBlob.Blob.CreateInStream(xlInStream);
        with rlVendor do begin
            if FindSet(false) then begin
                // Esta instrucción lo que hace es sacar todos los datos de la tabla en un archivo creado en memoria
                repeat
                    // Caso 1
                    CalcFields("Balance (LCY)");
                    // Tenemos que calcular el campo, ya que si no indicamos esta instrucción saldría 0
                    xlOutStream.WriteText("No." + ';');
                    xlOutStream.WriteText(Name + ';');
                    xlOutStream.WriteText(Address + ';');
                    xlOutStream.WriteText(City + ';');
                    xlOutStream.WriteText(format("Balance (LCY)"));

                    xlOutStream.WriteText(); // CR+LF
                    // Caso 2
                    xlOutStream.WriteText(StrSubstNo('%2%1%3%1%4%1%5%1%6',
                                                    ';',
                                                    "No.",
                                                    Name,
                                                    Address,
                                                    City,
                                                    "Balance (LCY)"));
                    xlOutStream.WriteText();
                until Next() = 0;
                CopyStream(xlOutStream, xlInStream);
                if DownloadFromStream(xlInStream, 'Fichero a descargar',
                                    '',
                                    'Archivos de texto (*.txt, *.csv)|*.txt;*.csv|Todos los archivos (*.*)|*.*',
                                    xlFichero) then begin
                    Message('Se ha descargado el fichero %1 correctamente', xlFichero);
                end else begin
                    Error('No se ha conseguido descargar el fichero %1', GetLastErrorText);
                end;

            end;
        end;
    end;

    local procedure ManejoFicherosF()
    var
        rlSalesShipmentLine: Record "Sales Shipment Line";
        rlSalesLine2: Record "Sales Line";
        rlSalesLine: Record "Sales Line";
        rlItem: Record Item;
        rlGLAccount: Record "G/L Account";
        rlTCNA: Record TCN_A;
        xlTCNColores: Enum TCN_Colores;
    begin
        with rlItem do begin
            SetCurrentKey(Description);
            // Para recorrer los items en función de, en este caso la descripción
            Ascending(false);
            // Siempre que se pueda utilizar un setrange
            SetRange("No.", '1000', '2000'); // Para indicar un filtro en un campo. 1er param es el campo, 2o la cifra inicial, 3o la cifra final
            SetRange("No."); // Para dejar en blanco el campo
            SetFilter("No.", '%1..%2|%3..%4', '1000', '1001', '2000', '2020'); // Para aplicar filtros de mayor complejidad
            // SetFilter("No.", '%1', Cadena); // Para obtener el filtro que pone el usuario
            Get();
            Find(); // Devuelve true si existe lo que estamos buscando. Respeta filtros
            FindFirst();
            FindLast();
            Next();
            Insert(); // True siempre cuando son campos reales (se tiene que ejecutar el postproceso); false si estamos trabajando con un registro temportal
            ModifyAll("Unit Price", 0, true); // Hay que indicar el campo, el nuevo dato y si queremos ejecutar el postproceso
            DeleteAll(); // Borra todos los registros de un plumazo, teniendo en cuenta los rangos o filtros indicados
        end;

        rlGLAccount.SetRange("Account Type", rlGLAccount."Account Type"::Posting, rlGLAccount."Account Type"::Heading);
        // En este caso, estamos filtrando un campo option (enum se comportaría igual), entonces no podemos escribir en el segundo parámetro
        // de la función .SetRange() un texto literal, deberé coger los campos posibles de ese campo, con lo cual tengo que cogerlo directamente

        rlTCNA.SetRange(Color, xlTCNColores::Verde);
        // Ejemplo con un enum


        // rlSalesLine.get(rlSalesLine."Document Type"::Order, '104001', 20000);

        // rlSalesLine."Document Type" := rlSalesLine."Document Type"::Order;
        // rlSalesLine."No." := '104001';
        // rlSalesLine."Line No." := 20000;
        // rlSalesLine.Find();
        rlSalesLine.SetRange("Document Type", rlSalesLine."Document Type"::Order);
        rlSalesLine.SetRange("Document No.", '104001');
        rlSalesLine.SetFilter("Qty. to Ship", '<1%', 0);
        // Se acumulan los filtros porque son campos distintos
        if rlSalesLine.FindSet(true) then begin
            repeat
                // Así no funciona
                // rlSalesLine.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                // rlSalesLine.Modify(true);

                // Solución 1
                rlSalesLine2.get(rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine."Line No.");
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                rlSalesLine2.Modify(true);

                // Solución 2
                rlSalesLine2 := rlSalesLine;
                rlSalesLine2.Find();
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                rlSalesLine2.Modify(true);

            until rlSalesLine.Next() = 0;
        end;
        // rlGLAccount.LockTable(); // Para bloquear la tabla mientras estás modificándola y no haya conflicto al solaparse las modificaciones
        // rlGLAccount.TransferFields(rlSalesLine); // Tienen que tener los mismos campos. Esta función copia los campos de la tabla indicada entre paréntesis en la que se indica anteriormente
        // Mira el número de campo, y si son de diferentes tipos nos salta error
        // if rlGLAccount.ReadPermission // Pregunta si el usuario tiene permisos de lectura para la tabla indicada
        // if rlGLAccount.WritePermission // Pregunta si el usuario tiene permisos de escritura para la tabla indicada        
    end;

    local procedure GruposFiltrosF()
    var
        xlGrupoFiltro: Integer;
        rlSalesHeader: Record "Sales Header";
    begin
        xlGrupoFiltro := rlSalesHeader.FilterGroup;
        // Obtenemos en nuestra variable el grupo de filtro actual (a nivel usuario)
        rlSalesHeader.FilterGroup(xlGrupoFiltro + 2);
        // Cambiamos el grupo de filtro, porque queremos filtrar y que el usuario no pueda cambiar dichos filtros
        // A continuación aplicamos los filtros que queremos en el grupo de filtro que hemos indicado
        rlSalesHeader.SetRange("Document Type", rlSalesHeader."Document Type"::Order);
        rlSalesHeader.FilterGroup(xlGrupoFiltro);
        // Reestauramos el grupo de filtro, en este punto habremos filtrado lo que queremos sin que el usuario pueda quitar estos filtros        
        rlSalesHeader.SetFilter("Sell-to Customer No.", '<>%1', '');
        if page.RunModal(0, rlSalesHeader) in [Action::LookupOK, Action::OK] then begin
            // En la función .RunModal, el primer parámetro indica la página que va a abrir, si indicamps un 0, se abrirá la página que tenga relaccionada la tabla en la propiedad LookUpPageId
            // Esta función, nos devuelve una ventana donde tendremos que aceptar            
            Message(rlSalesHeader."No.");
        end;
    end;

    local procedure GrupoFiltro2F()
    var
        plCustomerLookUp: Page "Customer Lookup";
        rlCustomer: Record Customer;
    begin
        // // En este ejemplo, modificamos el filtergroup 1, al no restaurarlo, cada vez que busquemos algo relaccionado con los clientes estará aplicado este filtro
        // // Nunca tenemos que trabajar con este grupo, siempre sumamos 2 y después lo restamos
        // rlCustomer.FilterGroup(1);
        // rlCustomer.SetRange("Salesperson Code", 'JR');
        // if Page.RunModal(0, rlCustomer) in [Action::LookupOK, Action::OK] then begin
        //     // Message(rlCustomer."No.");
        //     Message(rlCustomer.GetFilters);
        // end;
        rlCustomer.FilterGroup(rlCustomer.FilterGroup + 2);
        rlCustomer.SetRange("Salesperson Code", 'JR');
        rlCustomer.FilterGroup(rlCustomer.FilterGroup - 2);
        rlCustomer.get('27090917');
        plCustomerLookUp.SetTableView(rlCustomer);
        // Se le da la estructura de la tabla de ese registro
        plCustomerLookUp.SetRecord(rlCustomer);
        plCustomerLookUp.LookupMode(true);
        // Muestra el botón de aceptar        
        if plCustomerLookUp.RunModal() in [Action::LookupOK, Action::OK] then begin
            plCustomerLookUp.GetRecord(rlCustomer);
            // Se recoge el registro/os en la página indicada
            plCustomerLookUp.GetSelectionFilterF(rlCustomer);
            // Se recoge el filtro con la extensión de página que hemos hecho
            if rlCustomer.FindSet(false) then begin
                repeat
                    Message(rlCustomer."No.");
                until rlCustomer.Next() = 0;
            end;
            Message(rlCustomer.GetFilters);
        end;
    end;

    local procedure EjemploCalcSumF()
    var
        rlSalesLine: Record "Sales Line";
        rlTCNA: Record TCN_A;
    begin
        // CalcFormula = sum("Sales Line".Amount where ("Sell-to Customer No." = field (NumCliente),
        //                         "Shipment Date" = field(FiltroFecha),
        //                         "No." = field(FiltroProducto)));

        // Ejemplo CalcFields
        rlTCNA.SetRange(NumCliente, '10000');
        rlTCNA.SetFilter(FiltroFecha, '%1', DMY2Date(20, 1, 2021));
        rlTCNA.SetRange(FiltroProducto, 'LS-150');
        if rlTCNA.FindFirst() then begin
            rlTCNA.CalcFields(SumaImporte);
            Message(Format(rlTCNA.SumaImporte));
        end;

        // Ejemplo CalcSums
        rlSalesLine.SetRange("Sell-to Customer No.", '10000');
        rlSalesLine.SetFilter("Shipment Date", '%1', DMY2Date(20, 1, 2021));
        rlSalesLine.SetRange("No.", 'LS-150');
        rlSalesLine.CalcSums(Amount);
        Message(Format(rlSalesLine.Amount));
    end;

    procedure ImportePedidosFacturasClienteF(prCustomer: Record Customer; pDrillDown: Boolean) xSalida: Decimal
    var
        rlTempSalesLineTMP: Record "Sales Line" temporary;
        rlSalesLine: Record "Sales Line";
        rlSalesInvoiceLine: Record "Sales Invoice Line";
    begin
        with rlSalesLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");
            SetRange("Document Type", "Document Type"::Order);
            if pDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlTempSalesLineTMP.Init();
                        rlTempSalesLineTMP.TransferFields(rlSalesLine);
                        rlTempSalesLineTMP.Insert(false);
                    until Next() = 0;
                end;
            end else begin
                CalcSums(Amount);
            end;
            xSalida := Amount;
        end;
        with rlSalesInvoiceLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");
            if pDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlTempSalesLineTMP.Init();
                        rlTempSalesLineTMP.TransferFields(rlSalesInvoiceLine);
                        rlTempSalesLineTMP."Document Type" := rlTempSalesLineTMP."Document Type"::Invoice;
                        rlTempSalesLineTMP.Insert(false);
                    until Next() = 0;
                end;
            end else begin
                CalcSums(Amount);
            end;
            CalcSums(Amount);
            xSalida += Amount;
        end;
        if pDrillDown then begin
            Page.Run(0, rlTempSalesLineTMP);
        end;
    end;

    procedure NombreClienteF(pCodigo: Code[20]) xSalida: Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.get(pCodigo) then begin
            xSalida := rlCustomer.Name;
        end;
    end;

    local procedure EjemploExcelF()
    var
        xlNombreLibro: Text;
        xlHoja: Text;
        xlInStream: InStream;
        xlFichero: Text;
        rlTempExcelBuffer: Record "Excel Buffer" temporary;
    begin
        // Te abre la ventana para seleccionar el archivo deseado. Si hay sólo una hoja en ese fichero, arbirá directamente la que hay
        // De otra forma, si hay varias hojas, hay que seleccionar la deseada
        if UploadIntoStream('Seleccione libro Excel',
                             '',
                             'Archivos de texto (*.xls, *.xlsx)|*.xls;*.xlsx|Todos los archivos (*.*)|*.*',
                             xlFichero,
                             xlInStream) then begin
            xlHoja := rlTempExcelBuffer.SelectSheetsNameStream(xlInStream);
            xlNombreLibro := rlTempExcelBuffer.OpenBookStream(xlInStream, xlHoja);
            Message(xlNombreLibro);
            rlTempExcelBuffer.ReadSheet();
            // En esta línea, se lee la hoja que hemos seleccionado y la va recorriendo en el bucle que tenemos abajo
            if rlTempExcelBuffer.FindSet(false) then begin
                repeat
                    Message(StrSubstNo('Fila %1|Columna %2| Valor %3', rlTempExcelBuffer.xlColID,
                    rlTempExcelBuffer.xlRowID,
                    rlTempExcelBuffer."Cell Value as Text"));
                until rlTempExcelBuffer.Next() = 0;
            end;
        end else begin
            Error('No se ha podido subir el archivo Excel. Error(%1)', GetLastErrorText);
        end;
    end;
}