codeunit 60101 "TCN_SuscripcionesVar"
{
    SingleInstance = true;
    // EventSubscriberInstance = Manual;
    // La propiedad EventSubscriberInstance tiene el valor por defecto StaticAutomatic    

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnAfterInsertEvent', '', false, false)]
    // Este evento en concreto lo que hace es "saltar" si se inserta un nuevo cliente y mostramos un mensaje de confirmación
    local procedure MyProcedure(var Rec: Record Customer)
    begin
        if not Confirm(StrSubstNo('¿Desea insertar el cliente %1', Rec."No."), true) then begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::TCN_Lineas, 'OnAfterVerificarDatosLineasF', '', false, false)]
    local procedure OnAfterVerificarDatosLineasFTableTCNLineasF(var RecLineas: Record TCN_Lineas; xRecLineas: Record TCN_Lineas)
    begin
        if RecLineas.DescripcionVenta = '' then begin
            RecLineas.DescripcionVenta := 'Sin descripción';
        end;
        if RecLineas.DescripcionVenta = xRecLineas.DescripcionVenta then begin
            RecLineas.DescripcionVenta := 'Misma descripción';
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF', '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoFCodeunitTCNRegistrosVariosF(pMomentoFinalizacion: DateTime)
    // Traigo una variable, ya que el publicador nos manda una, del tipo DateTime. Ctrl + Espacio para que nos lo recomiende    
    // Te puedes suscribir a lo que tu quieras. Podríamos recoger solamente un parámetro de esta publicación
    var
        clMensaje: Label 'Se ha finalizado la ejecución en 2º plano en la fecha: %1';
    begin
        Message(clMensaje, pMomentoFinalizacion);
    end;

    [EventSubscriber(ObjectType::Page, Page::"Sales Quote", 'OnBeforeActionEvent', 'Statistics', false, false)]
    local procedure OnBeforeActionEventStatisticsPageSalesQuoteF()
    begin
        Message('Mi código');
        Commit();
        Error('');
        // Para que no ejecute la página que está por defecto, hacemos un commit y lanzamos un error sin mensaje    
    end;
}