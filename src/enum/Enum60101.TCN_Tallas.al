enum 60101 "TCN_Tallas"
{
    Extensible = true;

    value(0; " ")
    {
        Caption = ' ';
    }
    value(10; XS)
    {
        Caption = 'XS';
    }
    value(20; S)
    {
        Caption = 'S';
    }
    value(30; M)
    {
        Caption = 'M';
    }
    value(40; L)
    {
        Caption = 'L';
    }
    value(50; XL)
    {
        Caption = 'XL';
    }
    value(60; XXL)
    {
        Caption = 'XXL';
    }

}
