enum 60100 "TCN_Colores"
{
    Extensible = true;

    value(0; SinDefinir)
    {
        Caption = ' ';
    }
    value(5; Transparente)
    {
        Caption = 'Transparente';
    }
    value(10; Blanco)
    {
        Caption = 'Blanco';
    }
    value(20; Rojo)
    {
        Caption = 'Rojo';
    }
    value(30; Verde)
    {
        Caption = 'Verde';
    }
}