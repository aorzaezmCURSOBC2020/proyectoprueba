table 60102 "TCN_Ave"
{
    Caption = 'TCN_Ave';
    DataClassification = ToBeClassified;
    LookupPageId = TCN_ListaAves;
    DrillDownPageId = TCN_ListaAves;

    fields
    {
        field(1; Codigo; Code[10])
        {
            Caption = 'Codigo';
            DataClassification = ToBeClassified;
        }
        field(2; Nombre; Text[50])
        {
            Caption = 'Nombre';
            DataClassification = ToBeClassified;
            trigger OnValidate()
            var
                rlCustomer: Record Customer;
            begin
                Message('Has escrito %1 en el nombre del registro con código %2', Nombre, Codigo);
                rlCustomer.Init();
                rlCustomer."No." := Codigo;
                rlCustomer.Insert();
            end;
        }
        field(3; Preguntar; Boolean)
        {
            trigger OnValidate()
            var
                culSuscripcionesVar: Codeunit TCN_SuscripcionesVar;
            begin
                if Preguntar then begin
                    BindSubscription(culSuscripcionesVar);
                end else begin
                    UnbindSubscription(culSuscripcionesVar);
                end;
            end;
        }
        field(4; TipoPico; Code[10])
        {
            Caption = 'Tipo de pico';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pico));

        }
        field(5; TipoPluma; Code[10])
        {
            Caption = 'Tipo de pluma';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pluma));
        }
        field(6; TipoPata; Code[10])
        {
            Caption = 'Tipo de pata';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pata));
        }

    }
    keys
    {
        key(PK; Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Nombre)
        {

        }
    }
}