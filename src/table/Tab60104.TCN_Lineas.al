table 60104 "TCN_Lineas"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Codigo; Code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Nº línea';
        }
        field(3; FechaVenta; Date)
        {
            Caption = 'Fecha de venta';
        }
        field(4; DescripcionVenta; Text[250])
        {
            Caption = 'Descripción venta';
        }

    }

    keys
    {
        key(PK; Codigo, NumLinea)
        {
            Clustered = true;
        }
    }
    local procedure VerificarDatosF()
    var
        xlTexto: Text;
        // xl porque es variable local
    begin
        OnBeforeVerificarDatosLineasF();

        // 
        // 
        // 

        OnAfterVerificarDatosLineasF(Rec, xRec);
        // Rec es el registro actual en el que estoy
        // xRec es el registro de antes
    end;

    [IntegrationEvent(false, true)]
    local procedure OnBeforeVerificarDatosLineasF()
    begin
    end;

    [IntegrationEvent(false, true)]
    local procedure OnAfterVerificarDatosLineasF(var RecLineas: Record TCN_Lineas; xRecLineas: Record TCN_Lineas)
    begin

    end;

    trigger OnInsert()
    begin
        VerificarDatosF();
    end;

    var
        xTextoGlonal: Text;
        // Se define así porque es una variable global, omitimos la l del xl
        cMsgError: Label 'Proceso cancelado';
        // c al principio porque es una constante
        xVariable: array[10, 10] of Text;
        // Los arrays empiezan en 1. Se declaran las dimensiones en los corchetes
}