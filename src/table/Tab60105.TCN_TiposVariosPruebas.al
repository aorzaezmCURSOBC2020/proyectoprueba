table 60105 "TCN_TiposVariosPruebas"
{
    DataClassification = ToBeClassified;
    LookupPageId = TCN_ListaTiposVarios;

    fields
    {
        field(1; "Tipo"; Enum TCN_TipoCampo)
        {
            Caption = 'Tipo';
            DataClassification = ToBeClassified;
        }
        field(2; Codigo; Code[10])
        {
            Caption = 'Código';
        }
        field(3; Descripcion; Text[100])
        {
            Caption = 'Descripción';
        }
        field(4; Palmeada; Boolean)
        {
            Caption = 'Palmeada';
        }
    }

    keys
    {
        key(PK; "Tipo", Codigo)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }
}