table 60100 "TCN_A"
{
    DataClassification = ToBeClassified;
    Caption = 'Tabla A';
    LookupPageId = TCN_ListaA;
    // Hay que poner siempre una página en la que sourcetable sea esta misma.
    DrillDownPageId = TCN_ListaA;

    fields
    {
        field(1; Color; Enum TCN_Colores)
        {
            DataClassification = ToBeClassified;
        }
        field(2; Talla; Enum TCN_Tallas)
        {

        }
        field(3; DiaSemana; Option)
        {
            OptionMembers = Lunes,Martes,Miercoles,Jueves;
        }
        field(4; NumCliente; Code[20])
        {
            Caption = 'Número de cliente';
            TableRelation = Customer."No.";
            //  where ("Phone No." = filter (<> ''));
            // En el WHERE lo que estamos indicando que el No de telef sea distinto de un espacio en blanco.
            trigger OnValidate()
            begin
                TestField(Talla, Talla::XXL);
                // En este ejemplo, para rellenar el campo NumCliente, el campo talla tiene que tener el valor XXL
                Message('Has elegido el cliente %1', NumCliente);
            end;
        }
        field(5; Descripcion; Text[250])
        {
            trigger OnValidate()
            begin
                TestField(Talla);
                // Tiene que estar rellenado el campo indicado para que puedas rellenar este
                Descripcion := UpperCase(Descripcion);
            end;
        }
        field(6; TipoPersona; Enum TCN_TipoPersona)
        {
            Caption = 'Cliente o proveedor';
            InitValue = Cliente;
        }
        field(7; NumPersona; Code[20])
        {
            Caption = 'Nº cte/prov';
            TableRelation = if (TipoPersona = const (Cliente)) Customer."No." else
            Vendor."No.";
            trigger OnLookup()
            var
                plCustomerLookup: Page "Customer Lookup";
                plVendorLookup: Page "Vendor Lookup";
                rlCustomer: Record Customer;
                rlVendor: Record Vendor;
            begin
                case TipoPersona of
                    tipopersona::Cliente:
                        with rlCustomer do begin
                            if not get(rec.NumPersona) then begin
                                FindLast();
                            end;
                            // plCustomerLookUp.SetTableView(rlCustomer);
                            plCustomerLookUp.SetRecord(rlCustomer);
                            plCustomerLookUp.LookupMode(true);
                            plCustomerLookup.VisibilidadCreditoMaximoF(false);
                            if plCustomerLookUp.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plCustomerLookUp.GetRecord(rlCustomer);
                                rec.Validate(NumPersona, "No.");
                            end;
                        end;
                    TipoPersona::Proveedor:
                        with rlVendor do begin
                            if not get(rec.NumPersona) then begin
                                FindLast();
                            end;
                            // plCustomerLookUp.SetTableView(rlCustomer);
                            plVendorLookUp.SetRecord(rlVendor);
                            plVendorLookUp.LookupMode(true);
                            if plVendorLookUp.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plVendorLookUp.GetRecord(rlVendor);
                                rec.Validate(NumPersona, "No.");
                            end;
                        end;
                end;
            end;

            trigger OnValidate()
            begin
                if not (TipoPersona in [TipoPersona::Cliente, TipoPersona::Proveedor]) then begin
                    FieldError(TipoPersona, 'Debe ser un cliente o un proveedor');
                end;
            end;
        }
        field(8; SumaImporte; Decimal)
        {
            Caption = 'Suma de importe';
            FieldClass = FlowField;
            CalcFormula = sum ("Sales Line".Amount where ("Sell-to Customer No." = field (NumCliente),
                                "Shipment Date" = field (FiltroFecha),
                                "No." = field (FiltroProducto)));
        }
        field(9; FiltroFecha; Date)
        {
            Caption = 'Filtro fecha';
            FieldClass = FlowFilter;
        }
        field(10; FiltroProducto; Code[20])
        {
            Caption = 'Filtro producto';
            FieldClass = FlowFilter;
            TableRelation = Item."No.";
        }
        field(11; NombreCliente; Text[2048])
        {
            Caption = 'Nombre de cliente';
            FieldClass = FlowField;
            CalcFormula = lookup (Customer.Name where ("No." = field (NumCliente)));
        }
        field(12; CodAve; Code[10])
        {
            Caption = 'Código de ave';
            TableRelation = TCN_Ave.Codigo;
        }
        field(13; NombreAve; Text[2048])
        {
            Caption = 'Nombre ave';
            FieldClass = FlowField;
            CalcFormula = lookup (TCN_Ave.Nombre where (Codigo = field (CodAve)));
        }
    }

    keys
    {
        key(PK; Color)
        {
            // Cuando creamos la tabla, la primera key que declaramos es la clave primaria.
            Clustered = true;
        }
        key(Secundaria; NumCliente, Color, DiaSemana)
        {

        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin
        Message('Registro creado.');
    end;

    // trigger OnModify()
    // begin
    //     Error('No se permite cambio de clave');
    // end;
}