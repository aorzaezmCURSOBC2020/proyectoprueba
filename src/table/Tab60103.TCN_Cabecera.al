table 60103 "TCN_Cabecera"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Codigo; Code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
            TableRelation = TCN_Lineas.Codigo;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; Codigo)
        {
            Clustered = true;
        }
    }


}