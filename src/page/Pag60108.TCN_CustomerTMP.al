page 60108 "TCN_CustomerTMP"
{
    Caption = 'Clientes temporal';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Customer;
    SourceTableTemporary = true;
    // Utiliza la estructura de la tabla con la que se relaciona. Se borra cada vez que cerramos la página.
    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field("Customer Posting Group"; "Customer Posting Group")
                {
                    ApplicationArea = All;
                }
                field("Credit Limit (LCY)"; "Credit Limit (LCY)")
                {
                    ApplicationArea = All;
                }

            }
        }
    }
}