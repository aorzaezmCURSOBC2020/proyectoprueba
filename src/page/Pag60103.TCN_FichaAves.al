page 60103 "TCN_FichaAves"
{

    PageType = Card;
    SourceTable = TCN_Ave;
    Caption = 'Ficha de aves';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
            }
            group(Otros)
            {
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
