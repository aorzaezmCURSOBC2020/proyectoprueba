page 60104 "TCN_ListaLineas"
{
    Caption = 'Lista líneas';
    PageType = ListPart;
    SourceTable = TCN_Lineas;
    AutoSplitKey = true;
    PopulateAllFields = true;
    // Esta propiedad va a autorellenar los campos una vez se haya filtrado la lista. De otra
    // manera nos saltaría un aviso de que no se nos guardaría el nuevo registro.

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(DescripcionVenta; DescripcionVenta)
                {
                    ApplicationArea = All;
                }
                field(FechaVenta; FechaVenta)
                {
                    ApplicationArea = All;
                }
                field(NumLinea; NumLinea)
                {
                    ApplicationArea = All;
                }

            }
        }
    }
}