page 60109 "TCN_ListaTiposVarios"
{
    DataCaptionExpression = xDescLita;
    // Aquí, asignamos que nuestra variable se le añada a esta propiedad de la página, la cual modifica el "slug"
    Caption = 'Lista tipos varios';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = TCN_TiposVariosPruebas;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field(Codigo; Codigo)
                {
                    CaptionClass = StrSubstNo('3,Código %1', Tipo);
                    // En este propio campo, donde se indica el nombre del mismo, aparecerá también lo que le indiquemos en el CaptionClass
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(Palmeada; Palmeada)
                {
                    ApplicationArea = All;
                    Visible = xPalmeadaVisible;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }
    var
        xDescLita: Text;
        xPalmeadaVisible: Boolean;

    local procedure AsignarCamposVisiblesF()
    var
        xlGrupoFiltroInicial: Integer;
        i: Integer;
    begin
        xlGrupoFiltroInicial := FilterGroup;
        for i := 0 to 10 do begin
            // Se recorre los 11 diferentes tipos de grupos de filtro, porque no sabemos en cual está con el que estamos trabajando
            FilterGroup(i);
            // Obtengo el texto del tipo, si existe
            if GetFilter(Tipo) <> '' then begin
                xDescLita := StrSubstNo('Lista %s', GetFilter(Tipo));
            end;
            if GetFilter(Tipo) = Format(Tipo::Pata) then begin
                // Digo que si el tipo es pata me cambie la variable a true
                xPalmeadaVisible := true;
                i := 11;
            end;
        end;
        FilterGroup(xlGrupoFiltroInicial);
    end;

    trigger OnOpenPage()
    begin
        AsignarCamposVisiblesF();
        // Se llama a la función cuando se abre la página
    end;
}