page 60105 "TCN_Cabecera"
{
    Caption = 'Cabecera';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Documents;
    SourceTable = TCN_Cabecera;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
            }
            group(Informacion)
            {
                field(DescripcionInf; Descripcion)// El primero es un objeto página (se crea en el fichero página) y el segundo es objeto tabla porque hace referencia a un objeto de la tabla que se ha indicado en "sourcetable".
                {

                }
            }
            part(Lineas; TCN_ListaLineas)
            {
                SubPageLink = Codigo = field (Codigo);
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    Message('Hola clase');
                end;
            }
        }
        area(Navigation)
        {
            action(InfEmpresa)
            {
                Caption = 'Información empresa';
                Image = AddContacts;
                RunObject = page "Company Information";
                // Al hacer click en el botón nos va a llevar a esa página
            }
            action(ClientesVarios)
            {
                Caption = 'Clientes varios';
                Image = Customer;
                RunObject = page "Customer List";
                RunPageLink = "Country/Region Code" = filter ('*ES*');
            }
            action(PedirDatos)
            {
                Caption = 'Pedir texto proveedor';
                Image = "8ball";
                RunObject = page TCN_PedirTextoProveedor;
            }
            action(CreditoPorGrupoCliente)
            {
                Caption = 'Crédito por grupo de cliente';
                Image = Action;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    rlCustomerTMP: Record Customer temporary;
                begin
                    rlCustomer.FindSet(false);
                    repeat
                        rlCustomerTMP.SetRange("Customer Posting Group", rlCustomer."Customer Posting Group");
                        if not rlCustomerTMP.FindFirst() then begin
                            rlCustomerTMP.Init();
                            rlCustomerTMP."No." := rlCustomer."Customer Posting Group";
                            rlCustomerTMP."Customer Posting Group" := rlCustomer."Customer Posting Group";
                            rlCustomerTMP.Insert(false);
                        end;
                        rlCustomerTMP."Credit Limit (LCY)" += rlCustomer."Credit Limit (LCY)";
                        rlCustomerTMP.Modify(false);
                    until rlCustomer.Next() = 0;
                    Page.Run(page::TCN_CustomerTMP, rlCustomerTMP);
                end;
            }

        }
    }
}