page 60106 "TCN_PedirTextoProveedor"
{
    Caption = 'Pedir texto de proveedor';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Integer;
    SourceTableView = where (Number = const (0));
    // Tabla que contiene todos los enteros (tabla virtual)

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field(TextoAIntroducir; xTexto)
                {
                    Caption = 'Texto a introducir';
                    ApplicationArea = All;
                }

            }
        }
    }
    var
        xTexto: Text;
}