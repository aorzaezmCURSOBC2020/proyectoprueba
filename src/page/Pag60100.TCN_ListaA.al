page 60100 "TCN_ListaA"
{
    Caption = 'Lista A';
    PageType = List;
    SourceTable = TCN_A;
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(DiaSemana; DiaSemana)
                {
                    ApplicationArea = All;
                }
                field(TipoPersona; TipoPersona)
                {
                    ApplicationArea = All;
                }
                field(NumCliente; NumCliente)
                {

                    ApplicationArea = All;
                }
                field(NumPersona; NumPersona)
                {
                    ApplicationArea = All;
                }
                field(SumaImporte; SumaImporte)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }
                field(NombreCteFuncion; cuRegistrosVarios.NombreClienteF(NumCliente))
                {
                    Caption = 'Nombre cliente por función';
                }
                field(NombreCliente; NombreCliente)
                {
                    ApplicationArea = All;
                }
                field(CodAve; CodAve)
                {
                    ApplicationArea = All;
                }
                field(NombreAve; NombreAve)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.SetValorF('Hola');
                end;
            }
            action(GetValor)
            {
                trigger OnAction()
                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }
            action(EjecutarCodeUnit)
            {
                trigger OnAction()
                var
                    rlAveTMP: Record TCN_Ave temporary;
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    i: Integer;
                begin
                    // rlAveTMP.DeleteAll(false);
                    // No haría falta, ya que como es una tabla temporal, morirá
                    for i := 1 to 10 do begin
                        // Commit();                        
                        if not culRegistrosVarios.Run() then begin
                            rlAveTMP.Init();
                            rlAveTMP.Codigo := Format(i);
                            rlAveTMP.Nombre := GetLastErrorText;
                            rlAveTMP.Insert(true);
                        end;
                    end;
                    page.Run(0, rlAveTMP);
                    // Ejecuta la lista que tiene la tabla asignada con la propiedad LookUpPageId
                end;
            }
            action(Sobrecarga)
            {
                trigger OnAction()
                var
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    xlBigI: BigInteger;
                    xlFecha: Date;
                    xlDateFormula: Text;
                begin
                    xlBigI := 999;
                    xlFecha := Today;
                    xlDateFormula := '-1S';
                    Message(culRegistrosVarios.SobreCargaF(xlBigI));
                    Message(culRegistrosVarios.SobreCargaF(xlFecha));
                    Message(culRegistrosVarios.SobreCargaF(xlFecha, xlDateFormula));
                end;
            }

            action(SegundoPlano)
            {
                trigger OnAction()
                var
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    xlNumSesionIniciada: Integer;
                    xlSesionIniciada: Boolean;
                begin
                    culRegistrosVarios.EjecucionSegundoPlanoF();

                    // xlSesionIniciada := StartSession(xlNumSesionIniciada, Codeunit::TCN_RegistrosVarios);
                    // En esta instrucción creamos una nueva sesión que nos abre la codeunit indicada
                    // Esta se ejecutará en segundo plano
                    // Devuelve una variable boolean, el cual indica si se ha podido iniciar la sesión o no
                    // Message(Format(xlNumSesionIniciada));
                end;

            }
            action(PruebasVarias)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.Run();
                end;
            }
        }
    }
    var
        cuRegistrosVarios: Codeunit TCN_RegistrosVarios;
}
