page 60102 "TCN_ListaAves"
{
    PageType = List;
    SourceTable = "TCN_Ave";
    Caption = 'Lista aves';
    ApplicationArea = All;
    UsageCategory = Tasks;
    CardPageId = TCN_FichaAves;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
                field(Preguntar; Preguntar)
                {
                    ApplicationArea = All;
                }
                field(TipoPata; TipoPata)
                {
                    ApplicationArea = All;
                }
                field(TipoPico; TipoPico)
                {
                    ApplicationArea = All;
                }
                field(TipoPluma; TipoPluma)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}